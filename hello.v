// Example to demonstrate Icarus Verilog
// Taken from https://iverilog.fandom.com/wiki/Getting_Started

module hello;
  initial 
    begin
      $display("Hello, World");
      $finish ;
    end
endmodule