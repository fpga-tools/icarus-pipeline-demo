# Icarus Pipeline Demo
Demonstration of running Icarus Verilog within a CI/CD pipeline.

## How to run locally using Docker
For the Hello World example run the following command:
```
docker run --rm --volume `pwd`:/demo --workdir /demo -it registry.gitlab.com/docker-embedded/icarus-verilog-docker:latest iverilog -o hello hello.v && vvp hello
```
For the Counter example run the following command:
```
docker run --rm --volume `pwd`:/demo --workdir /demo -it registry.gitlab.com/docker-embedded/icarus-verilog-docker:latest iverilog -o my_design  counter_tb.v counter.v && vvp my_design
```